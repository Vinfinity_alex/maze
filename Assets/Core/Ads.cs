﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Ads : MonoBehaviour { 

    private GameManager _GameManager;
    public static int ExtraCoin;

    private void Awake()
    {
        _GameManager = GetComponent<GameManager>();
    }
    
    public void Show()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            _GameManager._UImanager.AdsGO.transform.localScale = Vector3.zero;
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                ExtraCoin = 5;
                GameManager.StarCount += ExtraCoin;
                _GameManager._UImanager.StarsCountUpdate();
                _GameManager._UImanager.FinalStarsTxt.text = (GameManager.MatchStarCount + ExtraCoin).ToString();
                break;
            case ShowResult.Skipped:
                ExtraCoin = 0;
                break;
            case ShowResult.Failed:
                ExtraCoin = 0;
                break;
        }
    }
}
