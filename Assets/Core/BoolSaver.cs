﻿using System;
using UnityEngine;

public class BoolSaver : ScriptableObject {
        
    internal static bool[] GetBoolVector(string key) {
        String[] chars = PlayerPrefs.GetString(key).Split(new char[] { ' ' });
        Boolean[] result = new Boolean[chars.Length-1];

        for (UInt16 i = 0; i < chars.Length-1; i++)
        {
            result[i] = chars[i+1] == "1" ? true : false;
        }
        return result;
    }

    internal static void SetBoolVector(String key, Boolean[] array)
    {
        if (array.Length > 0) {
            String result = "";
            for (UInt16 i = 0; i < array.Length; i++)
            {
                result += array[i] == true ? " 1" : " 0";
            }
            PlayerPrefs.SetString(key, result);
        }
    }

}
