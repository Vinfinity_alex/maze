﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Camera pivot class
public class CameraPivot : MonoBehaviour {
    [SerializeField]
    Transform Player;

	void Update () {
        //Translate Camera transform to Player
        transform.Translate((Player.transform.position - new Vector3(transform.position.x, transform.position.y))*Time.deltaTime);
	}
}
