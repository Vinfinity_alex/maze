﻿using UnityEngine;
using UnityEngine.UI;

public class ColorEditor : MonoBehaviour {


    internal const string COLOREDITOR = "_COLOREDITOR";
    internal const string GRADIENT = "_GRADIENT";
    internal const string DARKNESS = "_DARKNESS";

    [SerializeField]
    private Gradient myGradient;
    public Color color;
    [Range(0, 1)]
    public float time;
    [Range(0, 1)]
    public float darkness;

    private Image BtnBGImg;
    private Image ColPreviewImg;
    private Image DarkImg;
    private Slider ColorSlider;
    private Slider DarkSlider;
    private Image ColorSliderHandle;
    private Image DarkSliderHandle;
    internal Button ActiveBtn;


    private void Awake()
    {
        BtnBGImg = GameObject.Find("ColorEditorPanel(Clone)").GetComponent<Image>();
        ColPreviewImg = GameObject.Find("ColPreveiwImg").GetComponent<Image>();
        DarkImg = GameObject.Find("DarkBG").GetComponent<Image>();
        ColorSlider = GameObject.Find("ColorSlider").GetComponent<Slider>();
        DarkSlider = GameObject.Find("DarkSlider").GetComponent<Slider>();
        ColorSliderHandle = GameObject.Find("ColorSliderHandle").GetComponent<Image>();
        DarkSliderHandle = GameObject.Find("DarkSliderHandle").GetComponent<Image>();

        ActiveBtn = BtnBGImg.GetComponent<Button>();
        ActiveBtn.interactable = PlayerPrefs.HasKey(COLOREDITOR)?  true: false;

        ColorSlider.value = PlayerPrefs.HasKey(GRADIENT) ? PlayerPrefs.GetFloat(GRADIENT) : 0;
        DarkSlider.value = PlayerPrefs.HasKey(DARKNESS) ? PlayerPrefs.GetFloat(DARKNESS) : 1;

        //AddListeners
        ActiveBtn.onClick.AddListener(() => SetEditorActive(true));
        ColorSlider.onValueChanged.AddListener((float i) => ChangeColor());
        DarkSlider.onValueChanged.AddListener((float i) => ChangeColor());

        

        if (PlayerPrefs.HasKey(COLOREDITOR)) {
            if (PlayerPrefs.GetInt(COLOREDITOR) == 1)
            {
                SetEditorActive(true);
            }

            if (PlayerPrefs.GetInt(COLOREDITOR) == 0)
            {
                SetEditorActive(false);
            }
        }
    }

    internal void SetEditorActive(bool isActive)
    {
        ColorSlider.interactable = isActive;
        DarkSlider.interactable = isActive;
        
        if (isActive)
        {
            ChangeColor();
            BtnBGImg.color = GameManager.gameManager._Shop.ActiveItemColor;
            PlayerPrefs.SetInt(COLOREDITOR, 1);
            GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
        }
        else{
            BtnBGImg.color = GameManager.gameManager._Shop.BoughtItemColor;
            PlayerPrefs.SetInt(COLOREDITOR, 0);
        }
    }

    private void ChangeColor()
    {
        time = ColorSlider.value;
        darkness = DarkSlider.value;
        color = myGradient.Evaluate(time);

        ColorSliderHandle.color = color;
        Color greyscale = new Color(darkness, darkness, darkness);
        DarkSliderHandle.color = greyscale;

        DarkImg.color = color;
        color.r = color.r * darkness;
        color.g = color.g * darkness;
        color.b = color.b * darkness;

        ColPreviewImg.color = color;
        GameManager.gameManager._Shop.ChangeColor();
        PlayerPrefs.SetFloat(GRADIENT, time);
        Debug.Log(darkness.ToString());
        PlayerPrefs.SetFloat(DARKNESS, darkness);
    }
    
}
