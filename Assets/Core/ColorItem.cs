﻿using UnityEngine.UI;
using UnityEngine;

[SerializeField]
public class ColorItem : MonoBehaviour {
    public Image preview;
    public Text price;
    public Button btn;
    public int index;
}
