﻿using UnityEngine;

public class ExitPointer : MonoBehaviour {
    [SerializeField]
    GameObject Pointer;
    GameObject Finish;
    [SerializeField]
    Transform Cam;
    Vector3 FinishPosition;
    public bool isActive;

    public float OffsetX;
    public float ScreenFactor;

    void Start()
    {
        ScreenFactor = ((float)Screen.height) / ((float)Screen.width);
    }

    void Update ()
    {
        if (isActive)
        {
            PointerMove();
        }
    }

    public void SetFinish()
    {
        Pointer = GameObject.Find("Pointer");
        Finish = GameObject.Find("Finish(Clone)");
        FinishPosition = Pointer.transform.position;
        isActive = true;
        //Set pointer to center
        Pointer.transform.position = new Vector3(Cam.position.x, Cam.position.y);
    }

    void PointerMove()
    {
        //RaycastHit hit;
        //if (Physics.Raycast(Cam.position, Vector3.forward*100, out hit, 1000))
        //{
        //FinishGO.transform.position = new Vector3(Cam.position.x, Cam.position.y) + new Vector3(OffsetX, OffsetX * 0.5625f);

        if (Pointer.transform.position.x > -OffsetX)
            //&& Pointer.transform.position.x < OffsetX
            //&& Pointer.transform.position.x > -(OffsetX * ScreenFactor)
            //&& Pointer.transform.position.x < (OffsetX * ScreenFactor))
            
        
        
        // Разница двух  последовательно связаных векторов


        {
            Pointer.transform.Translate(
                (new Vector3(Finish.transform.position.x, Finish.transform.position.y) -
                (new Vector3(Pointer.transform.position.x, Pointer.transform.position.y))
                ) * Time.deltaTime / 10
                , Space.World);
        }
        //if (FinishGO.transform.position.x > -OffsetX && FinishGO.transform.position.x < OffsetX && FinishGO.transform.position.x > -(OffsetX * ScreenFactor) && FinishGO.transform.position.x < (OffsetX * ScreenFactor))
        //{
        //    
        //}

            

        //}
        //Debug.Log(hit.point.ToString());
    }
}
