﻿using UnityEngine;

// Finish tile class
public class Finish : MonoBehaviour {

    public static bool Finished = true;
    [SerializeField]
    AudioClip SuccessFX;

    // Checking for collision with Player
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) {
            GameManager.isWin = true;
            Finished = true;
            GameManager.FXSource.PlayOneShot(SuccessFX, 0.5f);
        }
    }
}
