﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    internal static GameManager gameManager;

    internal const String APPSTARTCOUNT = "_APPSTARTCOUNT";

    [SerializeField]
    public GameObject Player;

    [Space]
    [SerializeField]
    internal UIManager _UImanager;
    [SerializeField]
    internal ColorEditor _ColorEditor;
    [SerializeField]
    internal Shop _Shop;
    [SerializeField]
    internal ExitPointer _ExitPointer;
    [SerializeField]
    internal Purchaser _Purchaser;
    [SerializeField]
    internal Ads _Ads;
    [SerializeField]
    internal LabyrinthGenerator _LabyrinthGenerator;

    [Space]
    [SerializeField]
    internal Camera _MainCamera;

    public const Single MaxTimer = 120;
    public Single Timer = 0;

    public static Int32 AppStartsCounter;
    public static Int32 StarCount;
    public static Int32 MatchStarCount;

    public static Boolean isWin;

    public static Boolean isPaused;

    //Audio
    [Space]
    public static AudioSource FXSource;
    [SerializeField]
    internal AudioClip Fail;
    [SerializeField]
    internal AudioClip TickFX;
    [SerializeField]
    internal AudioClip BtnFX;


    private Button ColorEditorBtn;

    private bool isTickerActive;

    private void Awake()
    {
        gameManager = this;
        AppStartsCounter = PlayerPrefs.HasKey(GameManager.APPSTARTCOUNT) ? PlayerPrefs.GetInt(GameManager.APPSTARTCOUNT) : 0;
        AppStartsCounter++;
        PlayerPrefs.SetInt(GameManager.APPSTARTCOUNT, AppStartsCounter);
        Purchaser.ColorEditorActive += GetColorEditor;
    }

    private void Start()
    {
        FXSource = GetComponent<AudioSource>();
        ColorEditorBtn = _ColorEditor.ActiveBtn;
        Social.localUser.Authenticate(ProcessAuthentication);
    }

    void ProcessAuthentication(bool success)
    {
        if (success)
        {
            // Debug.Log("Authenticated, checking achievements");

            // Request loaded achievements, and register a callback for processing them
            Social.LoadAchievements(ProcessLoadedAchievements);
        }
        else
        {
            //Debug.Log("Failed to authenticate");
        }
    }

    void ProcessLoadedAchievements(IAchievement[] achievements)
    {
        if (achievements.Length == 0)
        {
            //Debug.Log("Error: no achievements found");
        }
        else
        {
            //Debug.Log("Got " + achievements.Length + " achievements");
        }

            // You can also call into the functions like this
            Social.ReportProgress("Achievement01", 100.0, result =>
            {
                if (result)
                    Debug.Log("Successfully reported achievement progress");
                else
                    Debug.Log("Failed to report achievement");
            });
    }
    
    private void Update()
    {
        TimerControl();
    }

    // Match time controll method
    private void TimerControl() {
        if (Timer > 0)
        {
            if (Finish.Finished)
            {
                Player.SetActive(false);
                //Counter.value = StarCount;
            }
            else
            {
                Timer -= Time.deltaTime;
            }

            if (Timer < 15.0f && !isTickerActive && !Finish.Finished)
            {
                StartCoroutine(TimerTicker(new WaitForSeconds(1.0f)));
                isTickerActive = true;
            }
        }
        else
        {
            if(!Finish.Finished) FXSource.PlayOneShot(Fail);
            Finish.Finished = true;
            Player.SetActive(false);
        }

        if (Finish.Finished)
        {
            StopAllCoroutines();
            isTickerActive = false;
        }


    }


    IEnumerator TimerTicker(WaitForSeconds wfs)
    {
        while (true)
        {
            FXSource.PlayOneShot(TickFX);
            yield return wfs;
        }
    }

    internal void GetColorEditor() {
        ColorEditorBtn.interactable = true;
        PlayerPrefs.SetInt(ColorEditor.COLOREDITOR, 0);
    }

}
