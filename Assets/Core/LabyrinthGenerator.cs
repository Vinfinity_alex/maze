﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

// Maze generator
public class LabyrinthGenerator : MonoBehaviour
{
    public enum CellState { Close, Open };

    // Cell class
    [Serializable]
    public class Cell
    {
        public Cell(Vector2 currentPosition)
        {
            Visited = false;
            Position = currentPosition;
        }
    
        public CellState Left { get; set; }
        public CellState Right { get; set; }
        public CellState Bottom { get; set; }
        public CellState Top { get; set; }
        public Boolean Visited { get; set; }
        public Vector2 Position { get; set; }
    }

    [SerializeField]
    private Int32 Width = 10 , Height = 10;
    [SerializeField]
    private Single Scale = 5.0f;
    public Int32 chance = 4;

    // Cells array
    private Cell[,] Cells;

    private Cell Finish;
    private Cell Start;

    [Space]
    //[SerializeField]
    //private GameObject TileObj;
    [SerializeField]
    private GameObject StarObj;
    [SerializeField]
    private GameObject WallObj;
    [SerializeField]
    private GameObject FinishObj;

    // Maze generation method by Growing Tree algorithm
    public void GenerateMaze(int _Width, int _Height)
    {
        Width = _Width;
        Height = _Height;

        Cells = new Cell[_Width, _Height];
    
        for (int y = 0; y < _Height; y++)
            for (int x = 0; x < _Width; x++)
                Cells[x, y] = new Cell(new Vector2(x, y));
    
        Random rand = new Random();
        Int32 startX = rand.Next(_Width);
        Int32 startY = rand.Next(_Height);
        Start = Cells[startX, startY];

        Stack<Cell> path = new Stack<Cell>();
    
        Cells[startX, startY].Visited = true;
        path.Push(Cells[startX, startY]);
        Int32 count = 0;
        Int32 max = 0;
        while (path.Count > 0)
        {
            Cell _cell = path.Peek();
    
            List<Cell> nextStep = new List<Cell>();
            if (_cell.Position.x > 0 && !Cells[Convert.ToInt32(_cell.Position.x - 1), Convert.ToInt32(_cell.Position.y)].Visited)
                nextStep.Add(Cells[Convert.ToInt32(_cell.Position.x) - 1, Convert.ToInt32(_cell.Position.y)]);
            if (_cell.Position.x < _Width - 1 && !Cells[Convert.ToInt32(_cell.Position.x) + 1, Convert.ToInt32(_cell.Position.y)].Visited)
                nextStep.Add(Cells[Convert.ToInt32(_cell.Position.x) + 1, Convert.ToInt32(_cell.Position.y)]);
            if (_cell.Position.y > 0 && !Cells[Convert.ToInt32(_cell.Position.x), Convert.ToInt32(_cell.Position.y) - 1].Visited)
                nextStep.Add(Cells[Convert.ToInt32(_cell.Position.x), Convert.ToInt32(_cell.Position.y) - 1]);
            if (_cell.Position.y < _Height - 1 && !Cells[Convert.ToInt32(_cell.Position.x), Convert.ToInt32(_cell.Position.y) + 1].Visited)
                nextStep.Add(Cells[Convert.ToInt32(_cell.Position.x), Convert.ToInt32(_cell.Position.y) + 1]);
    
            if (nextStep.Count > 0)
            {
                Cell next = nextStep[rand.Next(nextStep.Count)];
    
                if (next.Position.x != _cell.Position.x)
                {
                    if (_cell.Position.x - next.Position.x > 0)
                    {
                        _cell.Left = CellState.Open;
                        next.Right = CellState.Open;
                    }
                    else
                    {
                        _cell.Right = CellState.Open;
                        next.Left = CellState.Open;
                    }
                }
                if (next.Position.y != _cell.Position.y)
                {
                    if (_cell.Position.y - next.Position.y > 0)
                    {
                        _cell.Top = CellState.Open;
                        next.Bottom = CellState.Open;
                    }
                    else
                    {
                        _cell.Bottom = CellState.Open;
                        next.Top = CellState.Open;
                    }
                }
    
                next.Visited = true;
                path.Push(next);
                count++;
            }
            else
            {
                if (count > max) {
                    max = count;
                    Finish = path.Peek();
                }
                count = 0;
                path.Pop();
            }
        }
        
        MazeBuilding();
        TranslatePlayer();
    }

    // Method of maze creating.
    private void MazeBuilding()
    {
        Quaternion horizontalCellRot = new Quaternion();
        Quaternion verticalCellRot = Quaternion.Euler(0, 0, 90);

        //TileObj.transform.localScale = new Vector3(Scale, Scale, 1);
        WallObj.transform.localScale = new Vector3(Scale, WallObj.transform.localScale.y, 1);
        Destroy(GameObject.Find("Labyrinth"));
        GameObject Labyrinth = new GameObject("Labyrinth");

        Random star = new Random();
        int rand;

        for (int y = 0; y < Height; y++)
            for (int x = 0; x < Width; x++)
            {
                Vector3 pos = new Vector3(Cells[x, y].Position.x * Scale, Cells[x, y].Position.y * Scale, 1);

                if (Finish.GetHashCode() == Cells[x, y].GetHashCode())
                {
                    bool isSpawned = false;
                    if (Cells[x, y].Top == CellState.Open && !isSpawned)
                    {
                        Instantiate(FinishObj, pos, verticalCellRot, Labyrinth.transform);
                        isSpawned = true;
                    }

                    if (Cells[x, y].Left == CellState.Open && !isSpawned)
                    {
                        Instantiate(FinishObj, pos, horizontalCellRot, Labyrinth.transform);
                        isSpawned = true;
                    }

                    if (Cells[x, y].Right == CellState.Open && !isSpawned)
                    {
                        Instantiate(FinishObj, pos, Quaternion.Euler(0, 0, 180), Labyrinth.transform);
                        isSpawned = true;
                    }

                    if (Cells[x, y].Bottom == CellState.Open && !isSpawned)
                    {
                        Instantiate(FinishObj, pos, Quaternion.Euler(0, 0, -90), Labyrinth.transform);
                        isSpawned = true;
                    }
                }
                else
                {

                    // Star spawning
                    rand = star.Next(1, 100);
                    if (rand < chance && Cells[x,y] != Start) {
                        Instantiate(StarObj, pos, horizontalCellRot, Labyrinth.transform);
                    }
                }

                if (Cells[x, y].Top == CellState.Close)
                {
                    Instantiate(WallObj, pos + new Vector3(0, (-Scale) / 2.0f, -1), horizontalCellRot, Labyrinth.transform);
                }

                if (Cells[x, y].Left == CellState.Close)
                {
                    Instantiate(WallObj, pos + new Vector3(-Scale / 2.0f, 0, -1), verticalCellRot, Labyrinth.transform);
                }
                
                if (Cells[x, y].Right == CellState.Close)
                {
                    Instantiate(WallObj, pos + new Vector3(Scale / 2.0f, 0, -1), verticalCellRot, Labyrinth.transform);
                }

                if (Cells[x, y].Bottom == CellState.Close)
                {
                    Instantiate(WallObj, pos + new Vector3(0, (Scale) / 2.0f, -1), horizontalCellRot, Labyrinth.transform);
                }
            }
    }

    // Translate Player to maze start
    public void TranslatePlayer() {
        GameManager.gameManager.Player.transform.position = new Vector3(Start.Position.x * Scale, Start.Position.y * Scale);
    }
}
