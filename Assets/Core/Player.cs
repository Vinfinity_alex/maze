﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

// Player movement class
public class Player : MonoBehaviour {
     
	void FixedUpdate () {
        Move();
	}

    // Move method by CrossPlatformInput
    void Move() {
        if(CrossPlatformInputManager.GetAxis("Horizontal") != 0) transform.position  += new Vector3(CrossPlatformInputManager.GetAxis("Horizontal"), 0);
        if (CrossPlatformInputManager.GetAxis("Vertical") != 0) transform.position += new Vector3(0, CrossPlatformInputManager.GetAxis("Vertical"));
    }
}
