﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;


public class Purchaser : MonoBehaviour, IStoreListener
{
    public static event Action ColorEditorActive;

    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;


    public static string PRODUCT_150COINS = "150coins";
    public static string PRODUCT_400COINS = "400coins";
    public static string PRODUCT_1000COINS = "1000coins";
    public static string PRODUCT_2500COINS = "2500coins";
    public static string PRODUCT_4500COINS = "4500coins";
    
    void Start()
    {
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(PRODUCT_150COINS, ProductType.Consumable, new IDs() { { PRODUCT_150COINS, AppleAppStore.Name }, { PRODUCT_150COINS, GooglePlay.Name } });
        builder.AddProduct(PRODUCT_400COINS, ProductType.Consumable, new IDs() { { PRODUCT_400COINS, AppleAppStore.Name }, { PRODUCT_400COINS, GooglePlay.Name } });
        builder.AddProduct(PRODUCT_1000COINS, ProductType.Consumable, new IDs() { { PRODUCT_1000COINS, AppleAppStore.Name }, { PRODUCT_1000COINS, GooglePlay.Name } });
        builder.AddProduct(PRODUCT_2500COINS, ProductType.Consumable, new IDs() { { PRODUCT_2500COINS, AppleAppStore.Name }, { PRODUCT_2500COINS, GooglePlay.Name } });
        builder.AddProduct(PRODUCT_4500COINS, ProductType.Consumable, new IDs() { { PRODUCT_4500COINS, AppleAppStore.Name }, { PRODUCT_4500COINS, GooglePlay.Name } });

        UnityPurchasing.Initialize(this, builder);
    }

    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyProductID(string productId)
    {
        try
        {
            if (IsInitialized())
            {
                Product product = m_StoreController.products.WithID(productId);

                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }
        catch (Exception e)
        {
            Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);
        }

        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: Completed!");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_150COINS, StringComparison.Ordinal))
        {
            GameManager.StarCount += 150;
        }
        else if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_400COINS, StringComparison.Ordinal))
        {
            GameManager.StarCount += 400;
        }
        else if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_1000COINS, StringComparison.Ordinal))
        {
            GameManager.StarCount += 1000;
            ColorEditorActive();
        }
        else if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_2500COINS, StringComparison.Ordinal))
        {
            GameManager.StarCount += 2500;
            ColorEditorActive();
        }
        else if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_4500COINS, StringComparison.Ordinal))
        {
            GameManager.StarCount += 4500;
            ColorEditorActive();
        }

        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}