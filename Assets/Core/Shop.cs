﻿using System;
using UnityEngine;

public class Shop : MonoBehaviour {
    
    const String BOUGHTCOLOR = "_BOUGHTCOLOR";
    const String CURCOLOR = "_CURCOLOR";

    [Serializable]
    public class Item{
        public int price;
        public bool isBought;
        public int index;
    }

    [Serializable]
    public class ColItem : Item
    {
        [SerializeField]
        public Color color;
        public ColorItem colorItem;
    }
    
    [SerializeField]
    private ColItem[] ColorItems;
    [Space]
    [SerializeField]
    internal Color BoughtItemColor;
    [SerializeField]
    internal Color ActiveItemColor;


    [Space]
    private Boolean[] BoughtItem;
    [Space]
    [SerializeField]
    internal int ColorEditorActive;

    [SerializeField]
    private GameObject ColorShopContent;
    [SerializeField]
    internal RectTransform ColorRectContent;
    [SerializeField]
    private GameObject ColorShopItem;
    [SerializeField]
    private GameObject ColorEditorItem;

    internal float ContentSize = 0;

    [Space]
    [SerializeField]
    private AudioClip ErrFX;


    private void Awake () {
        Initialized();
    }

    private void Initialized()
    {
        //Check if color Editor active
        ColorEditorActive = PlayerPrefs.HasKey(ColorEditor.COLOREDITOR) ? PlayerPrefs.GetInt(ColorEditor.COLOREDITOR) : 0;

        // Load bought items data
        BoughtItem = PlayerPrefs.HasKey(BOUGHTCOLOR) ? BoolSaver.GetBoolVector(BOUGHTCOLOR) : new Boolean[ColorItems.Length];
        // Check if added new items;
        if (BoughtItem.Length < ColorItems.Length)
        {
            Boolean[] boughtItem = new Boolean[ColorItems.Length];
            for (int i = 0; i < BoughtItem.Length; i++)
            {
                boughtItem[i] = BoughtItem[i];
            }
            BoughtItem = boughtItem;
        }
        // Black bought by default
        BoughtItem[0] = true;

        float size = 155;
        Vector3 position = new Vector3(216, -75, 0);
        Quaternion rotation = Quaternion.Euler(0, 0, 0);
        ContentSize = 0;

        // Add shop items
        for (int i = 0; i < ColorItems.Length; i++)
        {
            ContentSize += size;
            GameObject itemObj = Instantiate(ColorShopItem, ColorShopContent.transform);
            ColorItem item = itemObj.GetComponent<ColorItem>();

            ColorItems[i].colorItem = item;
            ColorItems[i].price = GetPrice(i, 5);
            // Load bought items data
            ColorItems[i].isBought = BoughtItem[i];
            if (ColorItems[i].isBought)
            {
                item.price.text = "";
                ColorItems[i].colorItem.btn.targetGraphic.color = BoughtItemColor;
            }
            else
            {
                item.price.text = ColorItems[i].price.ToString();
            }
            item.index = i;
            item.btn.onClick.AddListener(() => BuyOrUse(item.index));
            item.preview.color = ColorItems[i].color;
            itemObj.transform.localPosition = position;
            position -= new Vector3(0, size, 0);
        }

        // AddColorEditor
        ContentSize += size;
        GameObject _item = Instantiate(ColorEditorItem, ColorShopContent.transform);
        _item.transform.localPosition = position;

        // Resize UI rect
        ColorRectContent.sizeDelta = new Vector2(ColorRectContent.rect.width, ContentSize);

        // Active current color
        if (PlayerPrefs.HasKey(CURCOLOR))
        {
            int curCol = PlayerPrefs.GetInt(CURCOLOR);
            if (curCol < ColorItems.Length)
            {
                if (ColorEditorActive == 1)
                {
                    ChangeColor();
                }
                else
                {
                    ChangeColor(ColorItems[curCol].color, curCol);
                }
            }
            else
            {
                if (ColorEditorActive == 1)
                {
                    ChangeColor();
                }
                else
                {
                    ChangeColor(ColorItems[0].color, 0);
                }
            }
        }
        else
        {
            if (ColorEditorActive == 1)
            {
                ChangeColor();
            }
            else
            {
                ChangeColor(ColorItems[0].color, 0);
            }
        }
    }

    // Generate price
    private int GetPrice(int index, byte minPrice) {
        return (index>0)? (int)Mathf.Pow(minPrice*index, 1.55f) : minPrice;
    }

    public void BuyOrUse(int index) {
        if (ColorItems[index].isBought == false)
        {
            if (TryToBuy(ColorItems[index].price))
            {
                BoughtItem[index] = true;
                ColorItems[index].isBought = true;
                BoolSaver.SetBoolVector(BOUGHTCOLOR, BoughtItem);
                ChangeColor(ColorItems[index].color, index);
                ColorItems[index].colorItem.price.text = "";
                ColorItems[index].colorItem.btn.targetGraphic.color = ActiveItemColor;
                GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
            }
            else
            {
                GameManager.FXSource.PlayOneShot(ErrFX, 0.5f);
            }
        }
        else
        {
            ChangeColor(ColorItems[index].color, index);
            GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
        }
        GameManager.gameManager._ColorEditor.SetEditorActive(false);
    }

    bool TryToBuy(int price) {
        if (GameManager.StarCount >= price)
        {
            GameManager.StarCount -= price;
            GameManager.gameManager._UImanager.StarsCountUpdate();
            return true;
        }
        else
        {
            return false;
        }
    }

    void ChangeColor(Color color, int index)
    {
        for (byte i = 0; i < ColorItems.Length; i++)
        {
            if (ColorItems[i].isBought) ColorItems[i].colorItem.btn.targetGraphic.color = BoughtItemColor;
        }
        ColorItems[index].colorItem.btn.targetGraphic.color = ActiveItemColor;
        GameManager.gameManager.Player.GetComponent<MeshRenderer>().materials[0].SetColor("_Color", color);
        PlayerPrefs.SetInt(CURCOLOR, index);
    }

    internal void ChangeColor()
    {
        for (byte i = 0; i < ColorItems.Length; i++)
        {
            if (ColorItems[i].isBought) ColorItems[i].colorItem.btn.targetGraphic.color = BoughtItemColor;
        }
        GameManager.gameManager.Player.GetComponent<MeshRenderer>().materials [0].SetColor("_Color", GameManager.gameManager._ColorEditor.color);
    }

}
