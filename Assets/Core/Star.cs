﻿using UnityEngine;

public class Star : MonoBehaviour {
    [SerializeField]
    AudioClip FX;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) {
            GameManager.MatchStarCount++;
            Destroy(gameObject);
            GameManager.FXSource.PlayOneShot(FX, 0.5f);
        }
    }
}
