﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TimerColor : MonoBehaviour {
    [SerializeField]
    Image TimerImg;
    GameManager gameManager;

    // Use this for initialization
    void Start () {
        gameManager = GetComponent<GameManager>();
    }
	
	// Update is called once per frame
	void LateUpdate () {

        Color col = Color.green;
        col.b = 0.17f;
        col.r = (1 - (gameManager.Timer / GameManager.MaxTimer))*2;

        if (col.r >= 1.0f)
        {
            col.g = gameManager.Timer / GameManager.MaxTimer * 2;
        }
        TimerImg.color = col;
        
    }

}
