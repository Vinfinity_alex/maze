﻿using UnityEngine.UI;
using UnityEngine;

public class Tip : MonoBehaviour {

    [SerializeField]
    Text TipTxt;
    [SerializeField]
    Button TipBtn;
    [SerializeField]
    string TipText;
    [SerializeField]
    byte Count = 3;
    [SerializeField]
    AudioClip BtnFX;

    private void Start()
    {
        if (GameManager.AppStartsCounter <= Count)
        {
            gameObject.SetActive(true);
            TipTxt.text = TipText;
            TipBtn.onClick.AddListener(() => OnOKClick());
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    private void OnOKClick()
    {
        gameObject.SetActive(false);
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }
}
