﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

// UI Manager class
public class UIManager : MonoBehaviour
{
    const string WIN = "You won!";
    const string LOSE = "You lose =(";
    const string MAZETIP = "Maze complexity";

    const string SAVECOMPLEXITY = "Complexity";
    const string SAVEPROGRESS = "Progress";
    const string SAVESTARS = "Stars";

    [SerializeField]
    Animator Anim;

    Boolean isShopActive;
    Boolean isDonateActive;

    // UI components
    [Space]
    [SerializeField]
    Slider Size;
    const int MINSIZE = 7;
    const int STARTMAXSIZE = 10;

    [SerializeField]
    Image TimerImg;

    [SerializeField]
    Text SizeLbl;
    [SerializeField]
    Text SizeTip;
    [SerializeField]
    Text TimerTxt;
    [SerializeField]
    Text FinalTxt;
    [SerializeField]
    Text StarsTxt;
    [SerializeField]
    public Text FinalStarsTxt;
    

    [SerializeField]
    Button StartBtn;
    [SerializeField]
    Button NextLvlBtn;
    [SerializeField]
    Button PauseBtn;
    public GameObject AdsGO;


    [Space]
    [SerializeField]
    BlurOptimized Blur;
    Boolean isPlaying;
    

    private void Start()
    {
        Size.minValue = MINSIZE;
        Size.maxValue = (PlayerPrefs.HasKey(SAVEPROGRESS) ? PlayerPrefs.GetInt(SAVEPROGRESS) : STARTMAXSIZE);
        Size.value = (PlayerPrefs.HasKey(SAVECOMPLEXITY) ? PlayerPrefs.GetInt(SAVECOMPLEXITY) : MINSIZE);
        StarsTxt.text = (PlayerPrefs.HasKey(SAVESTARS) ? PlayerPrefs.GetInt(SAVESTARS) : 0).ToString();
        GameManager.StarCount = PlayerPrefs.HasKey(SAVESTARS) ? PlayerPrefs.GetInt(SAVESTARS) : 0;
    }

    // Maze generation method
    public void OnGenerateMaze()
    {
        GameManager.gameManager._LabyrinthGenerator.GenerateMaze((int)Size.value, (int)Size.value);
        PlayerPrefs.SetInt(SAVECOMPLEXITY, (int)Size.value);
    }

    // Start match method
    public void OnStartGame(bool isRestart)
    {
        AdsGO.transform.localScale = Vector3.one;
        if (!isRestart) OnGenerateMaze();
        GameManager.gameManager.Player.SetActive(true);
        Finish.Finished = false;
        isPlaying = true;
        GameManager.MatchStarCount = 0;
        Ads.ExtraCoin = 0;
        GameManager.gameManager._LabyrinthGenerator.TranslatePlayer();
        Blur.enabled = false;
        Anim.SetBool("Home", false);
        Anim.SetBool("Final", false);
        Anim.SetBool("Star", false);
        Anim.SetBool("Start", false);
        GameManager.gameManager.Timer = GameManager.MaxTimer;
        GameManager.isWin = false;
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
        GameManager.isPaused = false;
        Anim.SetBool("Pause", false);
        Time.timeScale = 1;
    }

    public void OnComplexitySelect()
    {
        Anim.SetBool("Start", true);
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }
    

    private void Update()
    {
        // If in playmode
        if (isPlaying)
        {
            // Checking for end of macth
            if(Finish.Finished)Final();
            TimerUpdate();
        }
    }

    // Timer update method
    private void TimerUpdate() {
        TimerImg.fillAmount = GameManager.gameManager.Timer / GameManager.MaxTimer;
        
        String minutesStr = Math.Truncate((GameManager.gameManager.Timer - (GameManager.gameManager.Timer % 60)) / 60).ToString();
        Single seconds = Mathf.Round((GameManager.gameManager.Timer % 60) - 0.5f);
        String secondsStr = ((seconds < 0)? 0: seconds).ToString("00");
        TimerTxt.text = minutesStr + ":" + secondsStr;
    }

    // Final match method
    public void Final()
    {
        if (GameManager.isWin)
        {
            FinalTxt.text = WIN;
            NextLvlBtn.interactable = true;
            if (Size.maxValue == Size.value)
            {
                Size.maxValue++;
                PlayerPrefs.SetInt(SAVEPROGRESS, (int)Size.maxValue);
            }
             
            GameManager.StarCount += GameManager.MatchStarCount;
            StarsCountUpdate();
            FinalStarsTxt.text = GameManager.MatchStarCount.ToString();
        }
        else {
            FinalTxt.text = LOSE;
            NextLvlBtn.interactable = false;
        }

        Blur.enabled = true;
        Anim.SetBool("Star", true);
        Anim.SetBool("Final", true);
        isPlaying = false;
    }

    // Home button method
    public void OnHome()
    {
        Anim.SetBool("Final", false);
        Anim.SetBool("Home", true);
        Anim.SetBool("Start", false);
        Anim.SetBool("Pause", false);
        isPlaying = false;
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }

    // Exit button method
    public void OnExit()
    {
        Application.Quit();
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }

    // Pause button method
    public void OnPause() {
        if (!Finish.Finished)
        {
            GameManager.isPaused = !GameManager.isPaused;
            Anim.SetBool("Pause", GameManager.isPaused);
            if (GameManager.isPaused)
            {
                Time.timeScale = 0;
                Blur.enabled = true;
            }
            else
            {
                Time.timeScale = 1;
                Blur.enabled = false;
            }

            GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
        }
    }

    // Sound button method
    public void OnSound() {
        GameManager.FXSource.mute = !GameManager.FXSource.mute;
    }

    // Game info
    public void OnInfo() {
        //Application.OpenURL();
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }

    // Slider method
    public void OnComplexityChange() {
        SizeLbl.text = "Level " + ((int)Size.value).ToString();
        PlayerPrefs.SetInt(SAVECOMPLEXITY, (int)Size.value);
    }

    // Next level button method
    public void OnNextLvl() {
        Size.value++;
        OnStartGame(false);
    }

    // Reset progress button
    public void OnResetProgress() {
        PlayerPrefs.DeleteAll();
        Size.value = MINSIZE;
        Size.maxValue = STARTMAXSIZE;
        SizeLbl.text = ((int)Size.value).ToString();
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
        GameManager.StarCount = 0;
        StarsCountUpdate();
    }

    // Ads method
    public void OnAdsPresent() {
        //Ads.ShowAd();
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }

    // Shop button method
    public void OnShop()
    {
        isShopActive = !isShopActive;
        GameManager.gameManager._Shop.ColorRectContent.sizeDelta = new Vector2(GameManager.gameManager._Shop.ColorRectContent.rect.width, GameManager.gameManager._Shop.ContentSize);
        Anim.SetBool("Shop", isShopActive);
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }

    // Donate button method
    public void OnDonate()
    {
        isDonateActive = !isDonateActive;
        StarsCountUpdate();
        Anim.SetBool("Donate", isDonateActive);
        GameManager.FXSource.PlayOneShot(GameManager.gameManager.BtnFX, 0.5f);
    }


    public void StarsCountUpdate()
    {
        PlayerPrefs.SetInt(SAVESTARS, GameManager.StarCount);
        StarsTxt.text = GameManager.StarCount.ToString();
    }
}
